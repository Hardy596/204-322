﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabEventClickBtn
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //
            button2.Click += Button2_Click;
            //
            button3.Click += delegate
            {
                MessageBox.Show("3-ый Способ");
            };
            //button3.Click += Button2_Click;
            //
            button4.Click += (s, e) => { MessageBox.Show("4-ый Способ"); };
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("2-ый Способ");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("1-ый Способ");
        }
    }
}
