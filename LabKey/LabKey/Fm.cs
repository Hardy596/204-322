﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabKey
{
    public partial class Fm : Form
    {
        public Fm()
        {
            InitializeComponent();

            this.KeyDown += Fm_KeyDown;
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode) 
            {
                case Keys.Left:
                    LblText.Text = "Left";
                    break;
                case Keys.Right:
                    LblText.Text = "Right";
                    break;
                case Keys.Up:
                    LblText.Text = "Up";
                    break;
                case Keys.Down:
                    LblText.Text = "Down";
                    break;
                case Keys.Space:
                    if (e.Shift)
                    {
                        LblText.Text = "Shift + Space";
                    }
                    else
                    {
                        LblText.Text = "Space";
                    }
                    break;
                case Keys.X:
                    LblText.Text = e.Shift ? "Shift + X" : "X";
                    break;
                default:
                    LblText.Text = $"Other: {e.KeyCode}";
                    break;
            }
        }
    }
}
